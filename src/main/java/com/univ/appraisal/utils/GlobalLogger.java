package com.univ.appraisal.utils;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class GlobalLogger {

    @Before("execution(* com.univ.appraisal.controller.*.*(..)) ||" +
            "execution(* com.univ.appraisal.service.*.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        final Logger logger = Logger.getLogger(joinPoint.getTarget().getClass());
        logger.info(joinPoint.getSignature().getName() + getParams(joinPoint));
    }

    private String getParams(JoinPoint joinPoint) {
        return "(" + Arrays.stream(joinPoint.getArgs()).map(Object::toString)
                .collect(Collectors.joining(", ")) + ")";
    }
}
