package com.univ.appraisal.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.univ.appraisal.entity.User;
import com.univ.appraisal.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;

@Service
public class TokenAuthenticationService {

    @Value("${spring.security.token-expiration-time}")
    private long EXPIRATION_TIME;

    @Value("${spring.security.secret-key}")
    private String SECRET_KEY;

    @Autowired
    private UserService userService;

    private final String TOKEN_PREFIX = "Bearer";
    private final String HEADER_STRING = "Authorization";
    private final String REQUEST_TYPE = "request-type";
    private final String AUTHORIZE_REQUEST = "authorize";

    @Transactional
    public void addAuthentication(HttpServletRequest request, HttpServletResponse response, String username) throws IOException {
        Instant expirationTime = Instant.now().plusSeconds(EXPIRATION_TIME);

        String JWT = Jwts.builder()
                .setSubject(username)
                .setExpiration(Date.from(expirationTime))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();

        User user = userService.getUserByLogin(username);

        if (request.getHeader(REQUEST_TYPE) != null &&
                request.getHeader(REQUEST_TYPE).equals(AUTHORIZE_REQUEST)) {
            ObjectMapper mapper = new ObjectMapper();
            response.getWriter().write(mapper.writeValueAsString(user));
        }
        response.setHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
    }

    public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            String login = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();

            if (login != null) {
                addAuthentication(request, response, login);
                UserDetails user = userService.loadUserByUsername(login);
                return new UsernamePasswordAuthenticationToken(user.getUsername(), null, user.getAuthorities());
            }
        }
        return null;
    }
}
