package com.univ.appraisal.security;

public class Credentials {

    private String login;

    private String password;

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
