package com.univ.appraisal.controller;

import com.univ.appraisal.dto.UserDto;
import com.univ.appraisal.entity.User;
import com.univ.appraisal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return ResponseEntity.ok(userService.getAll()
                .stream()
                .map(UserDto::new)
                .collect(Collectors.toList()));
    }

    @PostMapping("/signup")
    public ResponseEntity signup(@RequestBody User user) {
        try {
            userService.createUser(user);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/profile")
    public ResponseEntity<UserDto> getUser(Authentication authentication) {
        UserDto user = new UserDto(userService.getUserByLogin(authentication.getName()));
        return ResponseEntity.ok(user);
    }

    @PutMapping("/profile")
    public ResponseEntity updateUser(@RequestBody User user, Authentication authentication) {
        user.setLogin(authentication.getName());
        try {
            userService.updateUser(user);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }
}
