package com.univ.appraisal.controller;

import com.univ.appraisal.dto.ChallengeDto;
import com.univ.appraisal.entity.Challenge;
import com.univ.appraisal.service.ChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/challenges")
public class ChallengeController {

    @Autowired
    private ChallengeService challengeService;

    @GetMapping
    public ResponseEntity<List<ChallengeDto>> getAllChallenges() {
        List<ChallengeDto> challenges = challengeService.getAllChallenges()
                .stream()
                .map(ChallengeDto::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(challenges);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ChallengeDto> getChallengeById(@PathVariable Long id) {
        ChallengeDto challenge = new ChallengeDto(challengeService.getChallengeById(id));
        return ResponseEntity.ok(challenge);
    }

    @PostMapping
    public ResponseEntity createChallenge(@RequestBody Challenge challenge) {
        challengeService.createChallenge(challenge);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/my")
    public ResponseEntity<List<ChallengeDto>> getChallengesByLogin(Authentication authentication) {
        List<ChallengeDto> challenges = challengeService.getAllChallengesByUserLogin(authentication.getName())
                .stream()
                .map(ChallengeDto::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(challenges);
    }
}
