package com.univ.appraisal.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    @Column(unique = true)
    private String login;

    @NotEmpty
    private String password;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<UserRole> roles;

    private String avatar;

    private String interests;

    public void validate() {
        if (login == null) throw new IllegalArgumentException("User must be provided with login");
        if (password == null) throw new IllegalArgumentException("User must be provided with password");
        if (firstName == null) throw new IllegalArgumentException("User must be provided with first name");
        if (lastName == null) throw new IllegalArgumentException("User must be provided with last name");
    }

    public void mapUpdatedUser(User updatedUser) {
        firstName = updatedUser.getFirstName();
        lastName = updatedUser.getLastName();
        avatar = updatedUser.getAvatar();
        interests = updatedUser.getInterests();
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    public Set<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", roles=" + roles +
                ", avatar='" + avatar + '\'' +
                ", interests='" + interests + '\'' +
                '}';
    }
}
