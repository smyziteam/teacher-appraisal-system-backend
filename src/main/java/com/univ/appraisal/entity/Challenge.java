package com.univ.appraisal.entity;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "challenges")
public class Challenge {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty
    private String title;

    @NotEmpty
    private String description;

    @NotNull
    private BigDecimal betSum;

    @ManyToOne
    @JoinColumn(name = "user_creator")
    private User userCreator;

    @ManyToOne
    @JoinColumn(name = "user_acceptor")
    private User userAcceptor;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getBetSum() {
        return betSum;
    }

    public void setBetSum(BigDecimal betSum) {
        this.betSum = betSum;
    }

    public User getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(User userCreator) {
        this.userCreator = userCreator;
    }

    public User getUserAcceptor() {
        return userAcceptor;
    }

    public void setUserAcceptor(User userAcceptor) {
        this.userAcceptor = userAcceptor;
    }

    @Override
    public String toString() {
        return "Challenge{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", betSum=" + betSum +
                ", userCreator=" + userCreator.getLogin() +
                ", userAcceptor=" + userAcceptor.getLogin() +
                '}';
    }
}
