package com.univ.appraisal.repository;

import com.univ.appraisal.entity.Challenge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChallengeRepository extends JpaRepository<Challenge, Long> {

    List<Challenge> findAllByUserCreatorLoginOrUserAcceptorLogin(String userCreatorLogin, String userAcceptorLogin);
}
