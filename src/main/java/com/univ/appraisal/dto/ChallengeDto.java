package com.univ.appraisal.dto;

import com.univ.appraisal.entity.Challenge;

import java.math.BigDecimal;

public class ChallengeDto {

    private Long id;

    private String title;

    private String description;

    private BigDecimal betSum;

    private UserDto userCreator;

    private UserDto userAcceptor;

    public ChallengeDto(Challenge challenge) {
        id = challenge.getId();
        title = challenge.getTitle();
        description = challenge.getDescription();
        betSum = challenge.getBetSum();
        userCreator = new UserDto(challenge.getUserCreator());
        userAcceptor = new UserDto(challenge.getUserAcceptor());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getBetSum() {
        return betSum;
    }

    public void setBetSum(BigDecimal betSum) {
        this.betSum = betSum;
    }

    public UserDto getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(UserDto userCreator) {
        this.userCreator = userCreator;
    }

    public UserDto getUserAcceptor() {
        return userAcceptor;
    }

    public void setUserAcceptor(UserDto userAcceptor) {
        this.userAcceptor = userAcceptor;
    }
}
