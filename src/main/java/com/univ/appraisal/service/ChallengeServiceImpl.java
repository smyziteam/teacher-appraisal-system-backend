package com.univ.appraisal.service;

import com.univ.appraisal.entity.Challenge;
import com.univ.appraisal.repository.ChallengeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChallengeServiceImpl implements ChallengeService {

    @Autowired
    private ChallengeRepository challengeRepository;

    @Override
    public List<Challenge> getAllChallenges() {
        return challengeRepository.findAll();
    }

    @Override
    public Challenge getChallengeById(Long id) {
        return challengeRepository.findOne(id);
    }

    @Override
    public void createChallenge(Challenge challenge) {
        challengeRepository.save(challenge);
    }

    @Override
    public List<Challenge> getAllChallengesByUserLogin(String login) {
        return challengeRepository.findAllByUserCreatorLoginOrUserAcceptorLogin(login, login);
    }
}
