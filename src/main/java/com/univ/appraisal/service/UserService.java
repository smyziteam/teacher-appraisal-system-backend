package com.univ.appraisal.service;

import com.univ.appraisal.entity.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User getUserByLogin(String login);

    List<User> getAll();

    void createUser(User user);

    void updateUser(User user);
}
