package com.univ.appraisal.service;

import com.univ.appraisal.entity.Challenge;

import java.util.List;

public interface ChallengeService {

    List<Challenge> getAllChallenges();

    Challenge getChallengeById(Long id);

    void createChallenge(Challenge challenge);

    List<Challenge> getAllChallengesByUserLogin(String name);
}
