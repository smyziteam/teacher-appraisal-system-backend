/* FOR TESTING PURPOSES ONLY */

INSERT INTO users (id, login, password, first_name, last_name) VALUES (9000, 'admin', '$2a$10$HWG4Vk6pZ1iskVUOj8Cn6OMiFatesN966oVSJXfsotlREeXeL.gry', 'adminov', 'adminovich');
INSERT INTO users (id, login, password, first_name, last_name) VALUES (9001, 'dima', '$2a$10$HWG4Vk6pZ1iskVUOj8Cn6OMiFatesN966oVSJXfsotlREeXeL.gry', 'dimov', 'dimovich');

INSERT INTO challenges (id, title, description, bet_sum, user_creator, user_acceptor) VALUES (9000, 'Chllenge-1', 'desc-1', '12.33', 9000, 9001);